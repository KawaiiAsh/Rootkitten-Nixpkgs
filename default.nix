let
  nixpkgs = import <nixpkgs> { };
  allPkgs = nixpkgs // pkgs;
  callPackage = path: overrides:
    let f = import path;
    in f
    ((builtins.intersectAttrs (builtins.functionArgs f) allPkgs) // overrides);
  pkgs = with nixpkgs; {
    cookiemonster = callPackage ./pkgs/fonts/CookieMonster { };
    madhacker = callPackage ./pkgs/fonts/madhacker { };
    dudu-calligraphy = callPackage ./pkgs/fonts/dudu-calligraphy { };
    dudu-calligraphy-fixed-numbers =
      callPackage ./pkgs/fonts/dudu-calligraphy-fixed-numbers { };
    gothique-mn-regular = callPackage ./pkgs/fonts/gothique-mn { };
    polyglot = callPackage ./pkgs/misc/polyglot { };
    oreo-cursors = callPackage ./pkgs/themes/oreo-cursors { };
  };
in pkgs

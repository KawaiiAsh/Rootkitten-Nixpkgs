{ fetchzip }:

fetchzip {
  name = "madhacker";

  url = "https://dl.dafont.com/dl/?f=mad_hacker";

  postFetch = ''
    mkdir -p $out/share/fonts
    unzip -j $downloadedFile \*.ttf -d $out/share/fonts/truetype
  '';

  sha256 = "1vlnys1v7rwv2n0cs21q2zsvjnd94bh6syq9fm06wv4fxixsfvcy";
}

{ stdenv, fetchzip }:

fetchzip {
  name = "CookieMonster";

  url = "https://dl.dafont.com/dl/?f=cookiemonster";

  postFetch = ''
    mkdir -p $out/share/fonts
    unzip -j $downloadedFile \*.ttf -d $out/share/fonts/truetype
  '';

  sha256 = "07svhgvxy3r50nv6k7mgr98g8f13jiavxska6mr4235ji43apd39";
}

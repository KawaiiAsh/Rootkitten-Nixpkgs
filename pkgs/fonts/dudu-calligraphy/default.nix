{ stdenv, fetchzip }:

fetchzip {
  name = "dudu-calligraphy";

  url = "https://dl.dafont.com/dl/?f=dudu_calligraphy";

  postFetch = ''
    mkdir -p $out/share/fonts
    unzip -j $downloadedFile \*.ttf -d $out/share/fonts/truetype
  '';

  sha256 = "14yr6wdnasjw2xmvd4qvwd6d8qs71irnqbl76jp27ykia68dcmf0";
}

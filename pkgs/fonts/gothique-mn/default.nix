{ stdenv, pkgs }:
stdenv.mkDerivation {
  name = "gothique-mn-regular";

  src = ./gothique-mn-regular.zip;

  nativeBuildInputs = [ pkgs.unzip ];

  unpackCmd = ''
    mkdir -p ./share/fonts/truetype
    unzip -j $src \*.ttf -d ./share/fonts/truetype/
  '';

  installPhase = ''
    mkdir -p $out/share/fonts/truetype
    cp ./fonts/truetype/* $out/share/fonts/truetype/
  '';
}

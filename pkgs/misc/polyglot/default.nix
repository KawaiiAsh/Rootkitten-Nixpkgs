{ stdenv, pkgs, fetchFromGitHub }:

stdenv.mkDerivation rec {
  name = "polyglot";
  version = "1.4";
  src = fetchFromGitHub {
    owner = "ddugovic";
    repo = "polyglot";
    rev = "a7aa136d3f87279b0e87ad5f9330d179f40fafc1";
    sha256 = "0n3vx757d1ayhj2zdlwqplz07qdsamwqfgrsv69k4m6a2vm77y80";
  };
}

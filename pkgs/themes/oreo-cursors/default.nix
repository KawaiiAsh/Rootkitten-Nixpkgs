{ stdenv, pkgs, fetchFromGitHub }:
stdenv.mkDerivation rec {
  pname = "oreo-cursors";
  version = "1.0.0";

  src = fetchFromGitHub {
    owner = "varlesh";
    repo = pname;
    rev = "ffe91dfac2afe3b8b74f50e1de13ff959c76665c";
    sha256 = "1p00ly2m8hif1jj58nssvy5cipw58p2asirx0agvlp8jlq5gbchd";
  };

  nativeBuildInputs = with pkgs; [ inkscape xorg.xcursorgen icon-slicer ruby ];

  colorConfig = ''
    pink_dream = #ff00aa
  '';

  prePatch = ''
    echo "${colorConfig}" > generator/colours.conf
  '';

  buildPhase = ''
    ruby generator/convert.rb
    make build
  '';

  makeFlags = [ "PREFIX= DESTDIR=$(out)" ];
}

